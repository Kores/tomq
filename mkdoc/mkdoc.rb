#!/usr/bin/env ruby
# frozen_string_literal: true

require 'asciidoctor'
Asciidoctor.render_file('./doc/tomq.1.asciidoc', backend: :manpage)
