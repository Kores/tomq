# tomq

**tomq** is a simple tool that converts TOML to JSON and JSON to TOML.

## Table of Contents

- [Installation](#installation)
- [Examples](#examples)
  - [TOML to JSON](#toml-to-json)
  - [jq filters](#jq-filtering)
  - [Re-transcode to TOML](#re-trasconding-to-toml)
  - [JSON to TOML](#converting-json-to-toml)
  - [Handling `null` values](#handling-null-values)
  - [Fallback key](#providing-a-fallback-key)

## Installation

#### Nix and NixOS

##### Release

```shell
nix-env -f https://gitlab.com/Kores/tomq/-/archive/master/tomq-master.tar.bz2 -i tomq
```

##### Development

```shell
nix-env -f https://gitlab.com/Kores/tomq/-/archive/master/tomq-master.tar.bz2 -i tomq-bleeding
```

##### Additional packages

You may also want to install `jq` and `bat`:

```shell
nix-env -iA nixpkgs.jq nixpkgs.bat
```

They are not required in order to convert from TOML to JSON and vice versa,
but **jq** is required for filtering and **bat** is required for the `--bat` option.

##### Shell

You can easily try **tomq** with **jq** and **bat** using `nix-shell`:

```shell
nix-shell https://gitlab.com/Kores/tomq/-/archive/shell/tomq-shell.tar.bz2
```

#### Cargo

##### Release

```shell
cargo install tomq
```

##### Development

```shell
cargo install --git https://gitlab.com/Kores/tomq.git
```

#### Other distros

**tomq** was recently released, I'm still cleaning up the code and haven't had time to package it for other
distros. Once I feel comfortable, I will first submit it to [nixpkgs](https://github.com/NixOS/nixpkgs) and then
start packaging it for other distros.

## Examples

### TOML to JSON

*sh (and compatibles, like tcsh/csh/zsh/bash):*
```shell
cat <<EOF | tomq
title = "tomq example"
description = "tomq is extraordinary"
EOF
```

*fish:*
```fish
printf %s\n 'title = "tomq example"' 'description = "tomq is extraordinary"' | tomq
```

*nushell:*
```nu
'
title = "tomq example"
description = "tomq is extraordinary"
' | tomq
```

#### Extra

Converts all `Cargo.toml` files to JSON format by streaming the contents from the files
and piping to **jq** on demand.

```shell
fd -0 'Cargo.toml' | xargs -r0 tomq --input-files
```

### jq filtering

**requires `jq` binary to be installed and available in the PATH (or any default location)**

Applies a jq filter and outputs the JSON format.

```shell
tomq '.package' Cargo.toml
```

### *Re-trasconding* to TOML

```shell
tomq -R '.package' Cargo.toml
```

### Converting JSON to TOML

```shell
echo -ne '{"package": {"name": "tomq"}}' | tomq -T
```

#### Converting JSON to TOML (multi-document)

Converts from JSON to TOML using multi-document extension (non TOML spec compliant).

```shell
echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}' | tomq -T
```

> This is not compliant to the TOML specification, but preserves the original JSON structure.

#### Converting JSON to TOML (common key)

Converts from JSON to TOML using a common key (TOML spec compliant).

```shell
echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}' | tomq -T -U packages
```

##### Note

This is compliant to the TOML specification, but changes the JSON structure, which can be
recovered using **jq**:
```shell
echo -ne '{"package": {"name": "tomq", "version": "1.0.0"}}{"package": {"name": "tomq2", "version": "2.0.0"}}' | tomq -T -U packages | tomq '.packages[]'
```

### Handling `null` values

Turn `null` values into empty documents:

```shell
echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}{"foo": "bar"}' | jq '.package' | tomq -T -E
```

#### Note

And even with the fact that **tomq** *moslty* preserves the original JSON structure, 
TOML doesn't have a sense of null values, so turning the previous output back to JSON 
will result in an empty object:

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}{"foo": "bar"}' | jq '.package'

{
  "name": "tomq"
}
{
  "name": "tomq2"
}
null
```

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}{"foo": "bar"}' | jq '.package' | tomq -T -E

name = "tomq"

---
name = "tomq2"

---
```

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}{"foo": "bar"}' | jq '.package' | tomq -T -E | tomq

{
  "name": "tomq"
}
{
  "name": "tomq2"
}
{}⏎ 
```

### Providing a fallback key

For cases where the JSON output is not an object:

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}' | jq '.package.name'

"tomq"
"tomq2"
```

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}' | jq '.package.name' | tomq -T -K names

names = "tomq"

---
names = "tomq2"
```

In case there are `null` values, TOML can't represent them, so you will need the `-E` flag:

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}{"foo": "bar"}' | jq '.package.name'

"tomq"
"tomq2"
null
````

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}{"foo": "bar"}' | jq '.package.name' | tomq -T -E -K names

names = "tomq"

---
names = "tomq2"

---
````

`-U` may also be handy in those cases:

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}' | jq '.package.name' | tomq -T -E -K value -U names

[[names]]
value = "tomq"

[[names]]
value = "tomq2"
```

```shell
❯ echo -ne '{"package": {"name": "tomq"}}{"package": {"name": "tomq2"}}' | jq '.package.name' | tomq -T -E -K value -U names | tomq '.names[].value'

"tomq"
"tomq2"
```