{ pkgs ? import <nixpkgs> {},
  tomq ? import ./tomq.nix
}:
pkgs.mkShell {
  name="dev-environment";
  buildInputs = [
    tomq
    pkgs.bat
    pkgs.jq
  ];
}