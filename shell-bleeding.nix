{ pkgs ? import <nixpkgs> {},
  tomq ? import ./tomq-bleeding.nix
}:
pkgs.mkShell {
  name="dev-environment";
  buildInputs = [
    tomq
  ];
}