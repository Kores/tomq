use std::error::Error;
use std::fmt::{Display, Formatter};

#[derive(Debug)]
pub(crate) enum TomqError {
    Serde(serde_json::Error),
    SerdeWithContext(serde_json::Error, String),
    ChainedFailure(String, String),
    Toml(toml_edit::ser::Error),
    TomlFromJson(toml_edit::ser::Error, serde_json::Value),
    TomlDe(toml_edit::de::Error),
    TomlParse(toml_edit::TomlError),
    Io(std::io::Error),
    JqNotFound,
}

pub(crate) type Result<T> = std::result::Result<T, TomqError>;

impl Display for TomqError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            TomqError::Serde(e) => write!(f, "Serde error: {}", e),
            TomqError::SerdeWithContext(e, ctx) => write!(f, "Serde error: {}. Context: {ctx}", e),
            TomqError::Toml(e) => write!(f, "Toml error: {}", e),
            TomqError::TomlFromJson(e, json) => write!(f, "Toml error: {}. While processing {json}", e),
            TomqError::ChainedFailure(fst, s) => write!(f, "Chained failure (one failed because of another). First error: {fst}. Second error: {s}"),
            TomqError::TomlDe(e) => write!(f, "Toml deserialize error: {}", e),
            TomqError::TomlParse(e) => write!(f, "Toml parse error: {}", e),
            TomqError::Io(e) => write!(f, "Io error: {}", e),
            TomqError::JqNotFound => write!(f, "jq is not installed, tomq requires it in order to apply jq filters to tomq output"),
        }
    }
}

impl Error for TomqError {}

impl From<serde_json::Error> for TomqError {
    fn from(e: serde_json::Error) -> Self {
        Self::Serde(e)
    }
}

impl From<toml_edit::ser::Error> for TomqError {
    fn from(e: toml_edit::ser::Error) -> Self {
        Self::Toml(e)
    }
}

impl From<std::io::Error> for TomqError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl From<toml_edit::TomlError> for TomqError {
    fn from(e: toml_edit::TomlError) -> Self {
        Self::TomlParse(e)
    }
}

impl From<toml_edit::de::Error> for TomqError {
    fn from(e: toml_edit::de::Error) -> Self {
        Self::TomlDe(e)
    }
}
