use std::io::{Error, ErrorKind};
use std::path::PathBuf;

pub(crate) fn find_bin(name: &str) -> Result<PathBuf, Error> {
    {
        let mut path = std::env::current_exe()?;
        path.pop();
        path.push(&name);
        if path.exists() {
            return Ok(path);
        }
    }

    const DIRS: &[&str] = &[
        "/usr/local/bin/",
        "/usr/bin/",
        "/bin/",
        "/usr/local/sbin/",
        "/usr/sbin/",
        "/sbin/",
    ];

    for dir in DIRS {
        let mut path = std::path::PathBuf::from(dir);
        path.push(&name);
        if path.exists() {
            return Ok(path);
        }
    }

    Err(Error::new(ErrorKind::NotFound, format!("{} not found", name)).into())
}
