use std::fmt::Debug;

pub(crate) trait ShortDisplayExt {
    fn short_display(&self, size: usize) -> ShortDisplay<'_, Self>
    where
        Self: Sized;
}

impl<T> ShortDisplayExt for Vec<T> {
    fn short_display(&self, size: usize) -> ShortDisplay<'_, Self> {
        ShortDisplay {
            reference: self,
            size,
        }
    }
}

pub(crate) struct ShortDisplay<'a, T> {
    reference: &'a T,
    size: usize,
}

impl<'a, T: Debug> Debug for ShortDisplay<'a, Vec<T>>
where
    Vec<T>: Debug,
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.reference.len() > self.size {
            write!(f, "[")?;
            for x in 0..(self.size) {
                write!(f, "{:?}, ", self.reference[x])?;
            }
            write!(f, "... ({} more)]", self.reference.len() - self.size)
        } else {
            write!(f, "{:?}", self.reference)
        }
    }
}
