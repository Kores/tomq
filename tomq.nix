let
  pkgs = import <nixpkgs> {
    overlays = [
      (import (fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz"))
    ];
  };
  rb = pkgs.ruby.withPackages (ps: with ps; [ pkgs.asciidoctor ]);
in with pkgs;

rustPlatform.buildRustPackage rec {
  pname = "tomq";
  version = "0.1.1";
  nativeBuildInputs = [
    rb
    rust-bin.stable."1.63.0".default
    installShellFiles
  ];

  src = fetchFromGitLab {
    domain = "gitlab.com";
    owner = "Kores";
    repo = "tomq";
    rev = "1088536bdb9f57f973fb84c2a2d1e38ea5c0a059";
    sha256 = "sha256-V6Chu+sEmyPNJgl9MMyv7HWITzrlVyhn3YeE2O2Ee1M=";
  };

  cargoSha256 = "sha256-TFz5YWQnZ7InqAMEx6tx8RyCe4IIFgPEBMNyQzhQ/0E=";

  postBuild = ''
    asciidoctor --backend manpage doc/tomq.1.asciidoc
  '';

  postInstall = ''
    installManPage doc/tomq.1
  '';

  meta = with lib; {
    description = "converts TOML to JSON and JSON to TOML, with support to jq filter delegation";
    homepage = "https://gitlab.com/Kores/tomq";
    license = licenses.mit;
    maintainers = with maintainers; [
      {
        name = "Jonathan H. R. Lopes";
        github = "JonathanxD";
        githubId = 5360060;
        matrix = "@jonathanhrl:matrix.org";
        email = "jhrldev@gmail.com";
        keys = [{
            longkeyid = "rsa4096/0x87BBD20689589D05";
            fingerprint = "EECD 9DDD CB4A 18E6 0799  E990 20E4 D69C F582 8274";
        }];
      }
    ];
  };
}