let
  pkgs = import <nixpkgs> {
    overlays = [
      (import (fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz"))
    ];
  };
  rb = pkgs.ruby.withPackages (ps: with ps; [ pkgs.asciidoctor ]);
in with pkgs;

rustPlatform.buildRustPackage rec {
  pname = "tomq";
  version = "bleeding";
  nativeBuildInputs = [
    rb
    rust-bin.stable."1.75.0".default
    installShellFiles
  ];

  src = builtins.filterSource (path: type: type != "directory" || (baseNameOf path != "target" && baseNameOf path != ".git" && baseNameOf path != ".idea")) ./.;

  cargoLock = {
    lockFile = ./Cargo.lock;
  };

  postBuild = ''
    asciidoctor --backend manpage doc/tomq.1.asciidoc
  '';

  postInstall = ''
    installManPage doc/tomq.1
  '';

  meta = with lib; {
    description = "converts TOML to JSON and JSON to TOML, with jq filter delegation";
    homepage = "https://gitlab.com/Kores/tomq";
    license = licenses.mit;
    maintainers = with maintainers; [
      {
        name = "Jonathan H. R. Lopes";
        github = "JonathanxD";
        githubId = 5360060;
        matrix = "@jonathanhrl:matrix.org";
        email = "joniweb01@gmail.com";
        keys = [{
            longkeyid = "rsa3072/0x4DF5FC43FD4FE9CC";
            fingerprint = "B2C2 7303 C091 E72F C62A  AB69 4DF5 FC43 FD4F E9CC";
        }];
      }
    ];
  };
}
